import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PacienteComponent } from './pages/paciente/paciente.component';
import { PacienteEdicionComponent } from './pages/paciente/paciente-edicion/paciente-edicion.component';
import { ExamenComponent } from './pages/examen/examen.component';
import { ExamenEdicionComponent } from './pages/examen/examen-edicion/examen-edicion.component';
import { EspecialidadComponent } from './pages/especialidad/especialidad.component';
import { EspecialiadEdicionComponent } from './pages/especialidad/especialiad-edicion/especialiad-edicion.component';
import { MedicoComponent } from './pages/medico/medico.component';
import { Not403Component } from './pages/not403/not403.component';
import { ConsultaComponent } from './pages/consulta/consulta.component';
import { MedicoEdicionComponent } from './pages/medico/medico-edicion/medico-edicion.component';

const routes: Routes = [
  {path: 'not-403', component: Not403Component},
  {path: 'consulta', component: ConsultaComponent},
  {path:'medico', component:MedicoComponent, children:[
    {path:'nuevo', component:MedicoEdicionComponent},
    {path: 'edicion/:id', component:MedicoEdicionComponent}
  ]},
  {
    path: 'paciente', component: PacienteComponent, children:[
      {path: 'nuevo', component: PacienteEdicionComponent},
      {path: 'edicion/:id', component: PacienteEdicionComponent}

    ]
   
  },

  {
    path: 'examen', component: ExamenComponent, children:[
      {path: 'nuevo', component: ExamenEdicionComponent},
      {path: 'edicion/:id', component:ExamenEdicionComponent}
    ]

  },

    {
      path: 'especialidad', component:EspecialidadComponent, children:[
       {path: 'nuevo', component:EspecialiadEdicionComponent},
       {path: 'edicion/:id', component:EspecialiadEdicionComponent}
      ]
    }

    

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
