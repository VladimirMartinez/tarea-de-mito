import { Injectable } from '@angular/core';
import { Subject } from '../../../node_modules/rxjs';
import { Consulta } from '../_model/consulta';
import { HOST } from '../_shared/var.constant';
import { HttpClient } from '@angular/common/http';
import { Medico } from '../_model/medico';
import { ConsultaListaExamen } from '../_model/consultaListaExamen';

@Injectable({
  providedIn: 'root'
})
export class ConsultaService {

  cambioConsulta = new Subject<Consulta[]>();
  
  mensaje = new Subject<string>();

  url: string=`${HOST}/consultas`;

  constructor(private http: HttpClient) { }

  listarConsulta(){
    return this.http.get<Medico[]>(this.url);
  }

  listarConsultaPorId(id: number){
    return this.http.get<Medico>(`${this.url}/${id}`);

  }

  registrar(consultaDTO : ConsultaListaExamen){
    return this.http.post(this.url, consultaDTO);
  }

  modificar(medico: Medico){
    return this.http.put(this.url, medico);
  }

  eliminar(id : number){
    return this.http.delete(`${this.url}/${id}`);
  }
}
