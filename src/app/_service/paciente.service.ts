import { Injectable } from '@angular/core';
import { HOST } from '../_shared/var.constant';
import { HttpClient } from '@angular/common/http';
import { Paciente } from './../_model/paciente';
import { Subject } from '../../../node_modules/rxjs';

@Injectable({
    providedIn: 'root'
})
export class PacienteService {
    pacienteCambio = new Subject<Paciente[]>();
    
    mensaje= new Subject<string>();
    
    url: string = `${HOST}/pacientes`;
    constructor(private http: HttpClient) { }

    listarPacientes() {
        return this.http.get<Paciente[]>(this.url);
    }

    listarPacientePorId(id: number){
        return this.http.get<Paciente>(`${this.url}/${id}`);
    }

    registrar(paciente: Paciente){
        return this.http.post(this.url, paciente);
    }

    modificar(paciente: Paciente){
        return this.http.put(this.url, paciente);
    }
    eliminar(id: number){
        return this.http.delete( `${this.url}/${id}`);
    }

}