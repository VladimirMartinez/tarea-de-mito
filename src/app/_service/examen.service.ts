import { Injectable } from '@angular/core';
import { Subject } from '../../../node_modules/rxjs';

import { HOST } from '../_shared/var.constant';
import { HttpClient } from '@angular/common/http';
import { Examen } from '../_model/examen';

@Injectable({
  providedIn: 'root'
})
export class ExamenService {
  cambioExamen = new Subject<Examen[]>();

  mensaje = new Subject<string>();

  url: string= `${HOST}/examenes`;

  constructor(private http: HttpClient) { }

  listarExamenes(){
    return this.http.get<Examen[]>(this.url)
  }
  listarExamenesPorId(id: number){
    return this.http.get<Examen>(`${this.url}/${id}`);

  }
  registrar(examen: Examen){
    return this.http.post(this.url, examen);
  }
  modificar(examen: Examen){
    return this.http.put(this.url, examen);
  }
  eliminar(id: number){
    return this.http.delete(`${this.url}/${id}`);
  }
}
