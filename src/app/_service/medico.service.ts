import { Injectable } from '@angular/core';
import { Subject } from '../../../node_modules/rxjs';
import { Medico } from '../_model/medico';
import { $ } from '../../../node_modules/protractor';
import { HOST } from '../_shared/var.constant';
import { HttpClient } from '@angular/common/http';
import { Paciente } from '../_model/paciente';

@Injectable({
  providedIn: 'root'
})
export class MedicoService {
  medicoCambio = new Subject<Medico[]>();

  mensaje= new Subject<string>();

  url: string = `${HOST}/medicos`;


  constructor( private http: HttpClient) { }

  listarMedicos(){
    return this.http.get<Medico[]>(this.url);
  }

  listarMedicoPorId(id: number){
    return this.http.get<Medico>(`${this.url}/${id}`);
  }
  registrar(medico: Medico){
    return this.http.post(this.url, medico);
  }

  modificar(medico: Medico){
    return this.http.put(this.url, medico);
  }

  eliminar(id: number){
    return this.http.delete(`${this.url}/${id}`);
  }
}
