import { Component, OnInit } from '@angular/core';
import { Paciente } from '../../_model/paciente';
import { Especialidad } from '../../_model/especialidad';
import { Medico } from '../../_model/medico';
import { Consulta } from '../../_model/consulta';
import { Examen } from '../../_model/examen';
import { PacienteService } from '../../_service/paciente.service';
import { EspecialidadService } from '../../_service/especialidad.service';
import { MedicoService } from '../../_service/medico.service';
import { DetalleConsulta } from '../../_model/detalleConsulta';
import { MatSnackBar } from '../../../../node_modules/@angular/material';
import { ExamenService } from '../../_service/examen.service';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit {

  paciente: Paciente[] = [];
  especialidades: Especialidad[] = [];
  medicos: Medico[] = [];
  consulta: Consulta;
  examenes: Examen[] = [];

  detalleConsulta: DetalleConsulta[] = [];

  diagnostico: string;
  tratamiento: string;
  idPacienteSeleccionado: number;
  idEspecialidadSeleccionado: number;
  idMedicoSeleccionado: number;
  idExamenSeleccionado: number;

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();

  mensaje: string;

  constructor(private pacienteService: PacienteService, private medicoService: MedicoService, private examenService: ExamenService, 
    private especialidadService: EspecialidadService, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.listarPacientes();
    this.listarEspecilidad();
    this.listarMedicos();
    this.listarExamenes();
  }

  listarPacientes() {
    this.pacienteService.listarPacientes().subscribe(data => {
      this.paciente = data;
    });
  }

  listarEspecilidad() {
    this.especialidadService.listarEspecialidad().subscribe(data => {
      this.especialidades = data;
    });
  }

  listarMedicos() {
    this.medicoService.listarMedicos().subscribe(data => {
      this.medicos = data;
    });
  }

  listarExamenes() {
    this.examenService.listarExamenes().subscribe(data => {
      this.examenes = data;
    });
  }

  agregar() {
    console.log('agregar');
    if (this.diagnostico != null && this.tratamiento != null) {
      console.log('dentro de agregar');
      let det = new DetalleConsulta();
      det.diagnostico = this.diagnostico;
      det.tratamiento = this.tratamiento;
      this.detalleConsulta.push(det);
      this.diagnostico = null;
      this.tratamiento = null;
    } else {
      this.mensaje = `Debe agregar un diagnóstico y tramiento`;
      this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
    }
  }

  removerDiagnostico(index: number) {
    this.detalleConsulta.splice(index, 1);
  }
}
