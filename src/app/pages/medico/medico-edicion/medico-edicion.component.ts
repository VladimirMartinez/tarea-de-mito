import { Component, OnInit } from '@angular/core';
import { Medico } from '../../../_model/medico';
import { FormGroup, FormControl } from '../../../../../node_modules/@angular/forms';
import { MedicoService } from '../../../_service/medico.service';
import { ActivatedRoute, Router, Params } from '../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-medico-edicion',
  templateUrl: './medico-edicion.component.html',
  styleUrls: ['./medico-edicion.component.css']
})
export class MedicoEdicionComponent implements OnInit {

  id: number;
  medico: Medico;
  form: FormGroup;
  edicion: boolean = false;
  constructor( private medicoService: MedicoService, private route: ActivatedRoute, private router: Router) { 

  this.medico = new Medico();
  this.form = new FormGroup({
    'id' : new FormControl(0),
    'nombres': new FormControl(''),
    'apellidos': new FormControl(''),
    'cmp': new FormControl('')

  });
}
  ngOnInit() {
    this.route.params.subscribe((params: Params)=>{
      this.id = params['id'];
      this.edicion=params['id'] !=null;
      this.initForm();
    })
  }
  private initForm(){
    if(this.edicion){
      this.medicoService.listarMedicoPorId(this.id).subscribe(data=>{
        let id= data.idMedico;
        let nombres= data.nombres;
        let apellidos=data.apellidos;
        let cmp= data.cmp;

        this.form = new FormGroup({
          'id': new FormControl(id),
          'nombres': new FormControl(nombres),
          'apellidos': new FormControl(apellidos),
          'cmp': new FormControl(cmp)
        });
      })
    }
  }
  operar(){
    this.medico.idMedico=this.form.value['id'];
    this.medico.nombres= this.form.value['nombres'];
    this.medico.apellidos= this.form.value['apellidos'];
    this.medico.cmp=this.form.value['cmp'];
    
    if(this.edicion){
      this.medicoService.modificar(this.medico).subscribe(data=>{
        console.log(data);

        this.medicoService.listarMedicos().subscribe(medico=>{
          this.medicoService.medicoCambio.next(medico);
          this.medicoService.mensaje.next('se Modifico');
        });
      })
    }else{
      this.medicoService.registrar(this.medico).subscribe(data=>{
        this.medicoService.listarMedicos().subscribe(medico=>{
          this.medicoService.medicoCambio.next(medico);
          this.medicoService.mensaje.next('Se registro');
        })
      })
    }
    this.router.navigate(['medico']);
  }

}
