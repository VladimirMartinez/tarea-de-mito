import { Component, OnInit, ViewChild } from '@angular/core';
import { Medico } from '../../_model/medico';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { MedicoService } from '../../_service/medico.service';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styleUrls: ['./medico.component.css']
})
export class MedicoComponent implements OnInit {

  lista: Medico[]=[];

  displayedColumns=['idMedico', 'nombres','apellidos', 'acciones'];
  dataSource: MatTableDataSource<Medico>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  constructor(private medicoService: MedicoService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.medicoService.medicoCambio.subscribe(data=>{
      this.lista=data;
      this.dataSource= new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

      this.medicoService.mensaje.subscribe(data=>{
        this.snackBar.open(data, null,{duration:2000});
      })
    })
    this.medicoService.listarMedicos().subscribe(data=>{
      this.lista=data;
      this.dataSource= new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    })

    

  }
  applyFilter(filterValue: string){
    filterValue= filterValue.trim();
    filterValue=filterValue.toLowerCase();
    this.dataSource.filter= filterValue;
  }
  eliminar(idMedico: number){
    this.medicoService.eliminar(idMedico).subscribe(data=>{
      this.medicoService.listarMedicos().subscribe(data=>{
        this.lista=data;
        this.dataSource= new MatTableDataSource(this.lista);
        this.dataSource.paginator=this.paginator;
        this.dataSource.sort=this.sort;
      })

    })
  }

}
