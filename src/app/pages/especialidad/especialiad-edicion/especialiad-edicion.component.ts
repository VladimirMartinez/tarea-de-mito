import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '../../../../../node_modules/@angular/forms';
import { Especialidad } from '../../../_model/especialidad';
import { EspecialidadService } from '../../../_service/especialidad.service';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-especialiad-edicion',
  templateUrl: './especialiad-edicion.component.html',
  styleUrls: ['./especialiad-edicion.component.css']
})
export class EspecialiadEdicionComponent implements OnInit {

  id: number;
  especialidad: Especialidad;
  form: FormGroup;
  edicion: boolean=false;
  constructor(private especialidadService: EspecialidadService, private route: ActivatedRoute, private router: Router) { 
    this.especialidad=new Especialidad();
    this.form=new FormGroup({
      'id': new FormControl(0),
      'nombre': new FormControl('')
    })
  }


  ngOnInit() {
    this.route.params.subscribe((params : Params)=>{
      this.id=params['id'];
      this.edicion=params['id']!=null;
      this.initForm();
    })
  }
  private initForm(){
    if(this.edicion){
      this.especialidadService.listarEspecialidadPorId(this.id).subscribe(data=>{
       let id= data.idEspecialidad;
       let nombre = data.nombre;

        this.form=new FormGroup({
          'id': new FormControl(id),
          'nombre': new FormControl(nombre)
        })
      })
    }
  }
  operar(){
    this.especialidad.idEspecialidad=this.form.value['id'];
    this.especialidad.nombre=this.form.value['nombre'];
    if(this.edicion){
      this.especialidadService.modificar(this.especialidad).subscribe(data=>{
        this.especialidadService.listarEspecialidad().subscribe(especialidad=>{
          this.especialidadService.cambioEspecialidad.next(especialidad);
          this.especialidadService.mensaje.next('Se Modifico');
        })
      })
    }else{
      this.especialidadService.registrar(this.especialidad).subscribe(data=>{
       this.especialidadService.listarEspecialidad().subscribe(especialidad=>{
         this.especialidadService.cambioEspecialidad.next(especialidad);
         this.especialidadService.mensaje.next('Se Registro');
       })
      })
    }
    this.router.navigate(['especialidad']);
  }

}
